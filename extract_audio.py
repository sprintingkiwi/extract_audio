import os
import time

# Taking time
t = time.time()

# Get list of videos and rename them
extensions = ["WEBM", "MPG", "MP2", "MPEG", "MPE", "MPV", "OGG", "MP4", "M4V", "AVI", "WMV", "MOV", "QT", "FLV", "SWF"]
files = os.listdir(".")
videos = []
for file_name in files:
    if len(file_name.split(".")) > 1 and file_name.split(".")[1].upper() in extensions:
        new_name = file_name.replace(" ", "_")
        os.rename(file_name, new_name)
        videos.append(new_name)
print(videos)


def convert_video(video_name):
    # FFMPEG
    new_name = video_name.split(".")[0] + ".mp3"
    os.system("ffmpeg -i " + video_name + " -f mp3 -ab 192000 -vn " + new_name)
    # os.system('handbrake.exe -i ' + video_name + ' -o ' + new_name + ' --optimize --format "av_mp4"')    

def start_video_serial():
    for video_name in videos:
        convert_video(video_name)
    print("TIME: " + str(time.time() - t))
    input("FINITO. PREMI INVIO PER CHIUDERE.")


start_video_serial()